# Chapter 14

## Sec. 14.4.2

**1.** In Eq. (14.52), the factor of $`1/2`$ in front of the number of modes $`m_{k,\alpha}`$ is wrong:
```math
m_{k,\alpha} = \frac{4\pi k_\alpha^2\Delta k}{k_F^3} = \textcolor{red}{\frac1{2\pi^2}} V k_\alpha^2 \Delta k .
\tag{14.52-14.53}
```
The reason is that, in Eq. (14.54) and all following, the sums over $`\bm{k}`$ run over the whole Fourier-space volume, not half of it. 

*Background:* Since $`\delta(\bm{x})`$ is a real field, it should also be possible to only run over half of Fourier space without loss of any information, in which case the given value of $`m_{k,\alpha}`$ would be correct. In that case, however, one of the contributions to the four-point function Eq. (14.56) is not there, reducing its value and ultimately leading to a covariance with a $`1/m_{k,\alpha}`$ prefactor; i.e. numerically the same covariance as one obtains when running over the full Fourier domain and using the corrected $`m_{k,\alpha}`$ from above. See [here](https://www.personal.psu.edu/duj13/dissertation/djeong_diss_appF.pdf) for that derivation.

*Thanks to Beatriz Tucci and Donghui Jeong!*

**2.** Eqs. (14.54-55) (see also point 3. below) lack factors of $`1/K_{\rm grid}^6`$:
```math
\hat{P}_{\mathrm{g}}(k_{\alpha }) = \frac{1}{m_{k,\alpha } \textcolor{red}{K_{\rm grid}^6}} \sum _{
\bm{k}}^{||\bm{k}|-k_{\alpha }| < \Delta k/2} |\delta _{\mathrm{g}}(
\bm{k})|^{2} ,
\tag{14.54}
```
```math
\mathrm{Cov}_{\alpha \beta }
= \frac{1}{m_{k,\alpha }\textcolor{red}{K_{\rm grid}^6}}
\!\!
\sum _{\bm{k}}^{||\bm{k}|-k_{\alpha }| < \Delta k/2}
\!\!
\frac{1}{m_{k,\beta }\textcolor{red}{K_{\rm grid}^6}}
\!\!
\sum _{\bm{k}'}^{||\bm{k}'|-k_{\beta }| < \Delta k/2}
\!\!
\left  [\left \langle |\delta _{\mathrm{g}}(\bm{k})|^{2} |
\delta _{\mathrm{g}}(\bm{k}')|^{2} \right \rangle - \left \langle |
\delta _{\mathrm{g}}(\bm{k})|^{2}\right \rangle \left \langle |
\delta _{\mathrm{g}}(\bm{k}')|^{2} \right \rangle \right  ],
\tag{14.55}
```
while Eq. (14.57) lacks a factor of $`K_{\rm grid}^6`$:
```math
\left \langle \delta _{\mathrm{g}}(\bm{k}) \delta _{\mathrm{g}}(\bm{k}')
\right \rangle = \textcolor{red}{K_{\rm grid}^6} \delta _{\bm{k},-\bm{k}'} \, P_{\mathrm{g}}(
k) .
\tag{14.57}
```
These appear due to the discretization convention adopted in Eq. (14.49). 
Similarly, Eq. (14.58) is incorrect as written, and the sentence around it should be disregarded.

*Thanks to Taisei Terawaki and colleagues!*

**3.** In the definition in Ch. 1, as well as throughout Chs. 11 and 12, the **galaxy power spectrum is defined as including the noise contribution** $`P_{N}`$ (this is also standard now in a large fraction of the literature). Hence, the subtraction of $`\textcolor{red}{P_{N}}`$ in Eq. (14.54) should be dropped, as already done above. Similarly, $`P_{N}`$ should be dropped from the expressions in Eqs. (14.57), (14.59) and (14.60).


## Sec. 14.5

**Figure 14.4:** note that the contour shown corresponds to $`\Delta\chi^2 = 1`$, which is _not_ the two-dimensional "$`1\sigma`$" contour (the latter is defined by $`\Delta\chi^2 = 2.3`$).

Showing that the marginalized error on $`\lambda_1`$ in fact corresponds to the distance between the center of the ellipse and the extrema of the $`\Delta\chi^2=1`$ contour in the $`\lambda_1`$ direction is left as exercise to the reader!

*Thanks to Dragan Huterer! (note: the same also applies to the analogous figure in the first edition)*

## Exercises:

Exercise 14.4: above Eq. (14.78), "monopoles" --> "multipoles".

*Thanks to Haoran Yu!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
