# Modern Cosmology Ed. 2 Corrections

Listed below are the **known issues**. The modified parts are highlighted in $`\textcolor{red}{{\rm red}}`$. To submit further corrections, please [email us](mailto:modcosmology@gmail.com).

<!-- define KaTeX macros here -->
<!-- \gdef: global definition; see https://katex.org/docs/supported.html#macros -->
<!-- edit: does not seem to work at this point, sadly; so just copy this block into all equation blocks -->
```math
\global\def\v#1{\bm{#1}}
\global\def\vk{\v{k}}
```

## [Chapter 1](Chapter_1.md)

## [Chapter 2](Chapter_2.md)

## [Chapter 3](Chapter_3.md)

## [Chapter 4](Chapter_4.md)

## [Chapter 5](Chapter_5.md)

## [Chapter 6](Chapter_6.md)

## [Chapter 7](Chapter_7.md)

## [Chapter 8](Chapter_8.md)

## [Chapter 9](Chapter_9.md)

## [Chapter 10](Chapter_10.md)

## [Chapter 11](Chapter_11.md)

## [Chapter 12](Chapter_12.md)

## [Chapter 13](Chapter_13.md)

## [Chapter 14](Chapter_14.md)

## [Appendices](Appendices.md)

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
