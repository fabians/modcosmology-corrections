# Chapter 13

## Sec. 13.4

In the text above Eq. (13.28), the sentence should read: "Hence, we need the derivative of the **source position** $`\theta_{\textcolor{red}{S}}`$ with respect to the observed angle."

*Thanks to Haoran Yu!*

## Sec. 13.5

### Sec. 13.5.2

The first line of Eq. (13.56) should read (no square on the cosine)
```math
\langle \gamma_1(\bm{0}) \gamma_1(\bm{\theta}) \rangle - \langle \gamma_2(\bm{0}) \gamma_2(\bm{\theta}) \rangle
= \int \frac{d^2 l}{(2\pi)^2} e^{i l\theta\cos\phi_l} \textcolor{red}{\cos} 4\phi_l \, C_{EE}(l)
\tag{13.56}
```
The second line is correct.

*Thanks to Alejandro Aviles Cervantes!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
