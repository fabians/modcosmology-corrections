# Chapter 9

## Sec. 9.1

Box 9.1, below Eq. (9.3): if only the sine mode was present, the peaks would be at $`(\textcolor{red}{n+1/2})\pi/\omega`$.

*Thanks to Haoran Yu!*

## Sec. 9.3

### Sec. 9.3.2

WKB stands for "Wentzel–Kramers–Brillouin".

*Thanks to Haoran Yu!*

## Sec. 9.5

### Sec. 9.5.1

Fig. 9.9: note that the dotted line shows the **spherical** Bessel function.

*Thanks to Haoran Yu!*

### Sec. 9.5.2

There is a complex conjugate missing on the left-hand side of Eq. (9.69):
```math
\langle\Theta(\bm{k},\hat{\bm{p}})\Theta\textcolor{red}{{}^*}(\bm{k}',\hat{\bm{p}}') \rangle = \dots
\tag{9.69}
```

*Thanks to Jens Chluba (via whatsapp)! (note: this issue is also present in the first edition)*

## Sec. 9.6

### Sec. 9.6.1

Eq. (9.80) should read
```math
l(l+1)C(l)^{\rm SW} = \frac{\textcolor{red}{2\pi}}{25} \mathcal{A}_s.
\tag{9.80}
```
The updated Fig. 9.11 looks like this:

![](images/cltt_lowl_OL.png)

*Thanks to Teppei Okumura and students!*

## Exercises

Exercise 9.5: there should be no primes on the baryon-to-photon ratio:
```math
\def\dote#1{#1'}
{\rm Real:}\ \ -(\dote{B})^{2}+\frac{A''}{A}+\frac{\textcolor{red}{R}}{1+R}\frac{\dote{A}}{A}+k^{2}c_{s}^{2}=0
\tag{9.87}
```
```math
\def\dote#1{#1'}
{\rm Imaginary:}\ \ 2\dote{B}\frac{\dote{A}}{A}+B''+\frac{\textcolor{red}{R}}{1+R}\dote{B}=0.
\tag{9.88}
```

*Thanks to Ali Rida Khalifeh!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
