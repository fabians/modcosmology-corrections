# Chapter 6

## Sec. 6.4

In the line right *above* Eq. (6.49), it should read $` h_{00} = \textcolor{red}{0}`$ of course.

*Thanks to Teppei Okumura!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
