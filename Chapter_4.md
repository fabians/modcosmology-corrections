# Chapter 4

## Exercises

Exercise 4.8: $`n^{(0)}(x)=n^{(0)}_X(x)`$ denotes the equilibrium number density of the particle $`X`$. You are asked to find the value of the variable $`x`$ where the equality given in the problem holds.

*Thanks to Elena Pierpaoli and students!*

Exercise 4.10: the statement $`T_{\rm DM} \propto T^2`$ is incorrect in general; while $`T_{\rm DM} \propto a^{-2}`$ for non-relativistic DM after decoupling, the plasma temperature does not simply scale as $`T \propto a^{-1}`$ (see e.g. Excercise 4.9). Assuming $`T_{\rm DM} \propto T^2`$ will still give a rough order-of-magnitude answer, but it is also easily possible to answer the posed question without assuming this relation.

*Thanks to Colin Hill!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
