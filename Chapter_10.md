# Chapter 10

## Sec. 10.1

Fig. 10.1: notice that the polarizer sketched here actually has $`\phi=0`$.

*Thanks to Richard Michalak!*

## Sec. 10.3

Fig. 10.7: the unit vectors $`\hat{\bm{\epsilon}}'_1`$ and $`\hat{\bm{\epsilon}}'_2`$ should be swapped.

*Thanks to Marcello Sedita!*


##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
