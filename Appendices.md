# Appendices

## Appendix A

### Chapter 3

In the solution to Exercise 3.2, Eq. (A.52) should read
```math
R = \frac{\textcolor{red}{2}}{r^2}.
\tag{A.52}
```

*Thanks to Donghui Jeong! (note: this typo is also present in the first edition)*

### Chapter 4

In the solution to Exercise 4.9, the calculation of $`g_*`$ today is incorrect (text above Eq. (A.77)). Since $`g_*`$ is determined in terms of the comoving *entropy*, contributions with different temperatures $`T_i`$ scale as $`T_i^3`$, not $`T_i^4`$. Hence, the calculation above Eq. (A.77) becomes
```math
g_*^{\rm today} = 2+ \frac78\times3\times2\times\textcolor{red}{\left(\frac{4}{11}\right)} = \textcolor{red}{\frac{43}{11} = 3.91}.
```
Correspondingly, Eq. (A.78) becomes
```math
\frac{(a T)^3_{T = 10\,\rm GeV}}{(a_0 T_0)^3} = \frac{\textcolor{red}{3.91}}{86.25} = \frac1{\textcolor{red}{22}} ,
\tag{A.78}
```
and $`1/27`$ for $`g_*^{200\,\rm GeV} = 103.75`$.

*Thanks to Colin Hill! (note: this issue is also present in the first edition)*

### Chapter 6

In the solution to Exercise 6.3, Eq. (A.88) should read
```math
\hat\phi(\hat x) = \bar\phi(\hat t) + \textcolor{red}{\hat{\delta\phi}}(\hat{t}, \hat{\bm{x}}).
\tag{A.88}
```

*Thanks to Richard Michalak!*

### Chapter 7

Exercise 7.2, first line: "There are 411 photons per $`{\rm cm}^{\textcolor{red}{3}}`$ today".

*Thanks to Haoran Yu!*

### Chapter 8

Exercise 8.8 (b): below Eq. (A.107), this is a first-order equation for $`du/d\textcolor{red}{a}`$ of course.

*Thanks to Haoran Yu!*

Exercise 8.13 (a): in the second line of (A.110), the integral sign is missing:
```math
\sigma_{R}^{2}=\left\langle\left[\int
d^{3}x\,\delta(\bm{x})W_{R}(\bm{x})\right]^{2}\right\rangle
```
```math
= \left\langle\left[\textcolor{red}{\int}\frac{d^{3}k}{(2\pi)^{3}}{\delta}(\bm{k}){W}_{R}^{*}(\bm{k})\right]^{2}\right\rangle
\tag{A.110}
```

*Thanks to Calvin Leung!*

## Appendix C

### Section C.2

There are some wrong signs and $`i`$ in the explicit spherical harmonics:

```math
Y_{00}(\theta, \phi)=\frac{1}{\sqrt{4\pi}}
\tag{unchanged}
```
```math
Y_{10}(\theta,
\phi)=\sqrt{\frac{3}{4\pi}}\cos(\theta) 
\tag{\textcolor{red}{no $i$}}
```
```math
Y_{1,\pm 1}(\theta, \phi\rangle=\mp
\sqrt{\frac{3}{8\pi}}\sin(\theta)e^{\pm i\phi} 
\tag{\textcolor{red}{no $i$}}
```
```math
Y_{20}(\theta, \phi)=\sqrt{\frac{5}{16\pi}}\textcolor{red}{(3\cos^{2}\theta-1)}
```
```math
Y_{2,\pm 1}(\theta, \phi)=\textcolor{red}{\mp}
\sqrt{\frac{15}{8\pi}}\cos\theta\sin\theta e^{\pm
i\phi}
\tag{\textcolor{red}{no $i$}}
```
```math
Y_{2,\pm 2}(\theta, \phi)= \textcolor{red}{+}\sqrt{\frac{15}{32\pi}}\sin^{2}\theta
e^{\pm 2i\phi}. 
```

*Thanks to Teppei Okumura and students! (note: this issue is also present in the first edition)*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
