# Chapter 8

## Sec. 8.1

### Sec. 8.1.1

Last paragraph on p. 199, discussion of Fig. 8.1: the small-scale mode actually has $`k = 2\,{\rm Mpc}^{-1}`$, as labeled correctly in the figure (i.e. without the $`h`$ factor).

The second equality in Eq. (8.9) is lacking a $`1/\epsilon`$ factor:
```math
\Delta_{\rm L}^2(k,a) = \frac1\epsilon \int_{|\ln k'-\ln k|<\epsilon} \frac{d^3k'}{(2\pi)^3} P_{\rm L}(k',a) 
= \textcolor{red}{\frac1\epsilon} \int_{|\ln k'-\ln k|<\epsilon} k'^3\frac{dk'}{k'}\int \frac{d\Omega'}{(2\pi)^3} P_{\rm L}(k',a)
\tag{8.9}
```

*Thanks to Richard Michalak!*

### Sec. 8.1.2

Second paragraph of p. 202: the correct number for the comoving horizon at equality is $`\eta(a_{\rm eq}) = \textcolor{red}{77}\, h^{-1}{\rm Mpc} = 113\, \textcolor{red}{\rm Mpc}`$.

In **Figs. 8.4, 8.5, 8.7, 8.8, 8.12**, the units of the y axes read $`\textcolor{red}{\rm Mpc}`$ and $`\textcolor{red}{\rm Mpc}^{-1}`$, respectively (without the $`h`$ factors).

*Thanks to Richard Michalak!*

### Sec. 8.3.2

Eq. (8.60): $`D_+(a)`$ should **not be confused with** $`D_+(y) = y+2/3 = D_+(a)/a_{\rm eq}`$. This unfortunate clash of notation led to the issue in Eq. (8.68), see Sec. 8.4 below.

## Sec. 8.4

In the text below Eq. (8.66), the approximate equality is $`-4/\textcolor{red}{(9 y_m)}`$.

*Thanks to Morteza Mohseni!*

Eq. (8.68) is incorrect as written. The derivation up to here assumes $`D_+ = D_+(y)`$, which is equal to $`D_+(a)/a_{\rm eq}`$ (see above). The correct equation thus is
```math
\delta_{\rm c}(\bm{k},a) = \frac32 A \mathcal{R}(\bm{k}) \ln\left[\frac{4 Be^{-3}a_{\rm eq}}{a_H}\right] \frac{D_+(a)}{\textcolor{red}{a_{\rm eq}}}\qquad (a \gg a_{\rm eq}).
\tag{8.68}
```
Eq. (8.69) is correct.

*Thanks to Adrienne Erickcek! Note: this issue is also present in the first edition.*

## Exercises

Exercise 8.1: under (b), third line: multiply $`\textcolor{red}{\mathrm{Eq.\ (5.67)}}`$, not (5.73).

*Thanks to Haoran Yu!*

Exercise 8.11: the factor 3.2 in the first line of the equation is in the wrong place. The first line of the equation should read:
```math
k_{\rm fs}(a) = \sqrt{a^{-2} + m_{\nu}^2/[\,\textcolor{red}{3.2 T_{\nu,0}}\,]^2}\: a^2 H(a)
```
which yields the second line (actually, with the numerical value of $`3.2`$, it yields a prefactor of $` 0.062 h {\rm Mpc}^{-1}`$; the $`0.063`$ is obtained when using $`3.15`$ instead of $`3.2`$).

*Thanks to Donghui Jeong!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
