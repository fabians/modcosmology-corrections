# Chapter 1

## Sec. 1.4

Figure 1.7: the unit of the $`y`$-axis is mega-Jansky per steradian, where a Jansky is defined as $`1\ {\rm Jy} = 10^{-26}\ {\rm J}\ {\rm s}^{-1}\ {\rm m}^{-2}\ {\rm Hz}^{-1}`$.

*Thanks to Mike Mowbray!*

## Exercises

Exercise 1.1: precisely, the temperature given should be $`1.22\times 10^{19}\,{\rm GeV}/k_{\rm B}`$, i.e. the Planck temperature.

*Thanks to Mike Mowbray! (note: this issue is also present in the first edition)*
##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
