# Chapter 3

## Sec. 3.3

There is a missing term in Eq. (3.64):
```math
\frac{d}{d\lambda} \left[(1+\Phi) a\right] = P^0 a [ \textcolor{red}{(1+\Phi)} H + \dot{\Phi} ]
+ a P^k \Phi_{,k}.
\tag{3.64}
```
Eq. (3.67) already (silently) canceled a number of terms, so for clarity we give an intermediate expression here:
```math
\frac{dp^i}{d\lambda} 
=  (1-\Phi) \left\{ E (1-\Psi)  [ (1+\Phi) H + \dot{\Phi} ] p^i + p^k \Phi_{,k} \frac{p^i}{aE}\right\}
```
```math
\hspace*{4.3cm} - (1+\Phi) E \bigg\{ \frac{E}{a} \Psi_{,i}
+ 2\left(H + \dot{\Phi} \right) p^i (1-\Psi -\Phi) + \frac2{a} \frac{p^i}{E} p^k \Phi_{,k} - \frac{p^2}{a E} \Phi_{,i} \bigg\}.
\tag{3.67a}
```
*Thanks to Teppei Okumura!*

## Exercises

Exercise 3.13: the expression $`4k^2\Phi/a^2`$ is in Fourier space (see box 5.1 in Sec. 5.3). This is equivalent to the real-space expression $`\textcolor{red}{-4\nabla^2}\Phi/a^2`$, which is an equally valid solution to the exercise.

*Thanks to Haoran Yu!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
