# Chapter 2

### Sec. 2.4.6

Clarification: in the caption of Fig. 2.8, _decreasing_ and _increasing_ refer to decreasing and increasing **in time** (rather than redshift, which goes the opposite way).

*Thanks to Richard Michalak!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
