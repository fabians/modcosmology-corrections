# Chapter 12

## Sec. 12.2

Eq. (12.39) should read 
```math
\def\v#1{\bm{#1}}
\def\vk{\v{k}}
%
F_2(\vk_1,\vk_2)
= \frac57 + \frac27 \frac{(\vk_1\cdot\vk_2)^2}{k_1^2k_2^2}
+ \frac12 \textcolor{red}{\frac{\vk_1\cdot\vk_2}{k_1 k_2}}
\left(\frac{k_1}{k_2} + \frac{k_2}{k_1}\right)
\tag{12.39}
```
```math
\def\v#1{\bm{#1}}
\def\vk{\v{k}}
%
G_2(\vk_1,\vk_2) = \frac37
+ \frac47 \frac{(\vk_1\cdot\vk_2)^2}{k_1^2 k_2^2}
+ \frac12 \textcolor{red}{\frac{\vk_1\cdot\vk_2}{k_1 k_2}}
\left(\frac{k_1}{k_2} + \frac{k_2}{k_1}\right).
```
*Thanks to Ivana Babić!*

## Sec. 12.3

The second line in Eq. (12.54) is missing a factor of m and should read:
```math
\nabla^2\Psi = 4\pi G a^2 \Big[ \textcolor{red}{m} \int \frac{d^3 p}{(2\pi)^3} f_{\rm m}(\bm{x}, \bm{p}, t) - \rho_{\rm m}(t) \Big] .
\tag{12.54}
```
*Thanks to Ali Rida Khalifeh!*

## Sec. 12.4

### Sec. 12.4.2

In the text above Eq. (12.70), it should read "and variance $`\sigma^{\textcolor{red}{2}}(R_L,z)`$, ...".

*Thanks to Haoran Yu!*

## Sec. 12.6

The derivation of the *peak-background split* bias parameter $`b_1`$, Eqs. (12.78-12.81), neglects the fact that the matter density in Eq. (12.78) is also perturbed, so that
```math
\frac{dn}{d\ln M}\Big|_{\delta_\ell} =\frac{\rho_{\rm m}(t_0) \textcolor{red}{(1+\delta_\ell^{(1)})}}{M}
f_{\rm PS}\left(\frac{\delta_{\rm cr}-\delta_\ell^{(1)}}{\sigma(M,z)}\right)
 \left|\frac{d\ln \sigma(M,z)}{d\ln M}\right|
```
```math
\hspace*{2.5cm}\approx \left. \frac{dn}{d\ln M}\right|_{0} \left[1 \textcolor{red}{+} \left(\textcolor{red}{1} -\frac{d\ln f_{\rm PS}}{d\nu} \frac1{\sigma(M,z)}\right) \delta_\ell^{(1)} \right]_{\nu = \delta_{\rm cr}/\sigma(M,z)} .
\tag{12.78}
```
Consequently, Eqs. (12.80-12.81) are corrected to
```math
\def\dcr{\delta_{\rm cr}}
b_1(M,z)= \textcolor{red}{1} - \left. \frac1{\sigma(M,z)} \frac{d\ln f_{\rm PS}(\nu)}{d\nu}
\right|_{\nu = \dcr/\sigma(M,z)} 
\tag{12.80}
```
```math
\def\dcr{\delta_{\rm cr}}
b_1^\text{PS}(M,z) = \textcolor{red}{1+} \left.\frac{\nu^2-1}{\dcr}\right|_{\nu = \dcr/\sigma(M,z)} 
\tag{12.81}
```
In fact, the bias parameter without the "+1" is known as *Lagrangian bias*, while the one including the "+1" is the *Eulerian bias* parameter. The former refers to the clustering of halos relative to the initial, unperturbed density field. Consequently, the thresholding argument we discuss afterwards derives the Lagrangian bias parameters which do not have the "+1" (cf. Eq. (A.158)). For detailed discussions of these subtle issues, see Sec. 2.1 and Sec. 3.3 of [this review](https://arxiv.org/abs/1611.09787).

*Thanks to Himanish Ganjoo!*

## Sec. 12.8

Same as in Eq. (12.54) above:
```math
\nabla^2\Psi = 4\pi G a^2 \Big[ \textcolor{red}{m} \int \frac{d^3 p}{(2\pi)^3} f_{\rm m}(\bm{x}, \bm{p}, t) - \rho_{\rm m}(t) \Big] .
\tag{12.98}
```

*Thanks to Morteza Mohseni!*

Below Eq. (12.101), "where $`\sigma(M,z)`$ ... is the **square-root of the** variance..."

*Thanks to Haoran Yu!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
