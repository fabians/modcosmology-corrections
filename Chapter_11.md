# Chapter 11

## Sec. 11.1

### Sec. 11.1.1 and 11.1.3 (1)

The definition of $`\delta H`$ given in the text after Eq. (11.12) and above Eq. (11.30) has the wrong sign. It should be analogous to the definition of $`\delta\chi`$ in Eq. (11.2), i.e.
```math
\delta H(z) = H_{\rm fid}(z) - H(z).
```
Correspondingly, there are several sign changes in the following equations in Sec. 11.1.3 (including fixing another error in Eq. (11.32)):
```math
x^3(z) = \left[1 \textcolor{red}{+} \frac{\delta H(\bar z)}{H_{\rm fid}(\bar z)} \right] x^3_{\rm obs} .
\tag{11.30}
```
```math
\alpha_\perp = \frac{\delta\chi}{\chi_{\rm fid}}\bigg|_{\bar z} ; \quad
\alpha_\parallel = \textcolor{red}{-} \frac{\delta H}{H_{\rm fid}}\bigg|_{\bar z} .
\tag{11.31}
```
```math
\chi(\bar z) = \chi_{\rm fid}(\bar z) \left[1 \textcolor{red}{-} \alpha_\perp \right] .
\tag{11.32}
```
```math
H(\bar z) = H_{\rm fid}(\bar z) \left[1 + \alpha_\parallel \right] .
\tag{11.33}
```
See also the separate issue discussed next.

*Thanks to Teppei Okumura and Federico Marulli!*

### Sec. 11.1.1 and 11.1.3 (2)

The discussion below Eq. (11.14) neglects the fact that the mean galaxy density $`\bar n_{g,\rm obs}`$ is obtained in _fiducial_ coordinates. That is,
```math
\bar n_{g,\rm obs} = \langle n_{g,\rm obs} \rangle_{z_{\rm obs}\  \rm fixed} = \bar n_g \bar J,
```
where in the last equality we have expanded to linear order, while the text implicitly set $\bar n_{g,\rm obs} = \bar n_g$. This means that there is no $`\bar J`$ factor in Eq. (11.15):
```math 
1+\delta_{\mathrm{g},\rm obs}(\bm{x}_{\rm obs})=1+\delta_{\mathrm{g}}(\bm{x}[\bm{x}_{\rm obs}]) 
- \frac1{aH} \frac{\partial}{\partial x} u_\parallel(\bm{x}[\bm{x}_{\rm obs}]).
\tag{11.15}
 ```

 This implies several corrections to the following text:
- A few references to "We set $`\bar J`$ to unity." are now unnecessary, likewise the sentence above Eq. (11.34).
- Eq. (11.34) now becomes
```math
\delta_{g,\rm obs}(\bm{k}_{\rm obs}) 
= \int d^{3} x_{\rm obs} \ 
e^{-i\bm{k}_{\rm obs}\cdot\bm{x}_{\rm obs}}\delta_{\mathrm{g},\rm RSD}(\bm{x}[\bm{x}_{\rm obs}])
```
```math
= (1+\alpha_\perp)^2(1+\alpha_\parallel) \int d^{3}x\ 
e^{-i\bm{k}[\bm{k}_{\rm obs}]\cdot\bm{x}}\delta_{\mathrm{g},\rm RSD}(\bm{x}[\bm{x}_{\rm obs}])
```
```math
= \textcolor{red}{\bar J^{-1}} \delta_{g,\rm RSD}(\bm{k}[\bm{k}_{\rm obs}]) .
\tag{11.34}
```
That is, there is a prefactor $`(1+\alpha_\perp)^2(1+\alpha_\parallel) = \bar J^{-1}`$. The number conservation argument does not hold, because we define the mean galaxy density differently in observed coordinates (see above).
- Eq. (11.36) becomes
```math
\delta_{g,\rm obs}(\bm{k}_{\rm obs}) = \textcolor{red}{\bar J^{-1}} [b_1 +f\mu_k^{2}]\delta_{\rm m}(\bm{k})\Big|_{\bm{k}=\left( [1+\alpha_\perp] k_{\rm obs}^1,\  [1+\alpha_\perp] k_{\rm obs}^2,\  [1+\alpha_\parallel] k_{\rm obs}^3\right)}.
\tag{11.36}
```
- Finally, when computing the power spectrum, we also have to take into account the transformation of the Dirac delta $`(2\pi)^3 \delta_{\rm D}(\bm{k}_{\rm obs}-\bm{k'}_{\rm obs})`$ from observed coordinates (with respect to fiducial cosmology) to $`\bm{k}`$, which yields a factor
```math
\left|\frac{\partial^3 k_{\rm obs}}{\partial^3 k}\right| = \bar J, 
```
so the prefactor in the power spectrum is $\bar J^{-2} \cdot \bar J$, and we finally obtain
```math
P_{\mathrm{g},\rm obs}(\bm{k}_{\rm obs},\bar z)=
\textcolor{red}{(1+\alpha_\perp)^2(1+\alpha_\parallel)} \left[
P_{\rm L}(k,\bar z)\left[b_1 +f\mu_k^{2}\right]^{2}
\bigg|_{\bm{k}=\left( [1+\alpha_\perp] k_{\rm obs}^1,\  [1+\alpha_\perp] k_{\rm obs}^2,\  [1+\alpha_\parallel] k_{\rm obs}^3\right)} + P_N
\right].
\tag{11.37}
```
Since this is a constant prefactor to the power spectrum, it of course does not affect the discussion of BAO scale inference in the following text.

Finally, note that in the literature (e.g., Beutler et al (2017)) a different convention is used for the rescaling factors, specifically
```math
\alpha_\perp^{\rm Lit.} = 1 - \alpha_\perp^{\rm Mod. Cosm.} ; \quad
\alpha_\parallel^{\rm Lit.} = 1 - \alpha_\parallel^{\rm Mod. Cosm.} .
```

*Thanks to Masahiro Takada and Ryo Terasawa!*

### Sec. 11.1.2

In Eq. (11.19), the derivative should be $`\partial/\partial\textcolor{red}{x}`$, not $`\partial/\partial\bm{x}`$.

*Thanks to Haoran Yu!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
