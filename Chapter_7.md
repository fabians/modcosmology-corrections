# Chapter 7

## Sec. 7.1

In the text above Eq. (7.1), it should read $`\eta_* \approx \textcolor{red}{190} \,h^{-1}{\rm Mpc}`$ *or* $`\eta_* \approx 281\,{\rm Mpc}`$. 

Above Eq. (7.2), it should read $`\eta_0 \approx \textcolor{red}{9590} \,h^{-1}{\rm Mpc}`$ *or* $`\eta_0 \approx 14200\,{\rm Mpc}`$. Eq. (7.2) itself is not affected.

*Thanks to Richard Michalak!*

The angle calculated in Eq. (7.2) corresponds to the angular size of the horizon at recombination (shaded regions in Fig. 7.1). However, the angle separating two patches without _any causal overlap whatsoever_ is in fact 2 times larger than the angle in Eq. (7.2).

*Thanks to Lucas Cardoso Leão and colleagues!*

## Sec. 7.2

The equation of state in Eq. (7.10) should read
```math
w = \frac{\mathcal{P}}{\rho} = \frac{\dot\phi^2\textcolor{red}{/2} - V(\phi)}{\dot\phi^2\textcolor{red}{/2} + V(\phi)}
\tag{7.10}
```

*Thanks to Kevin Ludwick!*

## Sec. 7.4

The condition for adiabatic perturbations, Eq. (7.43) should read
```math
\frac{\delta\rho_s}{\textcolor{red}{\dot\rho_s}} 
= \frac{\delta\rho}{\textcolor{red}{\dot\rho}} .
\tag{7.43}
```
Using the unperturbed (zeroth order) continuity equation, this can be equivalently written as
```math
\frac{\delta\rho_s}{\rho_s+\mathcal{P}_s} 
= \frac{\delta\rho}{\rho+\mathcal{P}} ,
```
where $`\rho, \mathcal{P}`$ stand for the total energy density and pressure, respectively.

*Thanks to Elena Pierpaoli and students!*

### Sec. 7.4.2

At the end of the second paragraph after Eq. (7.56), reference should be made to **Eq. (7.56)**, rather than Eq. (7.11).

Figure 7.6: $`H_{\rm inf}`$ should be replaced by $`\textcolor{red}{a_e} H_{\rm inf}`$ in the legend and label of the x axis. Similarly, in the text above Eq. (7.60), it should read $`\textcolor{red}{a_e} H_{\rm inf} \eta \gtrsim -10^{4}`$.

*Thanks to Richard Michalak!*

### Sec. 7.4.3

In the last paragraph, at the top of p. 183: $` \mathcal{R} `$ is equal to $`\textcolor{red}{-}\Phi_H`$ in a gauge where velocities vanish.

*Thanks to Richard Michalak!*

## Exercises

Exercise 7.4: Eq. (7.113) should read
```math
T_{\mu\nu} = \textcolor{red}{-2} \frac{\delta\mathcal{L}_\phi}{\delta g^{\mu\nu}} + g_{\mu\nu} \mathcal{L}_\phi.
\tag{7.113}
```

*Thanks to Yesukhei Jagvaral!*

Exercise 7.15: The definition of neutrino fraction is relative to the total _radiation_ rather than photon density:
```math
\mathfrak{f}_\nu \equiv \rho_\nu / \textcolor{red}{\rho_{\rm r}}.
```

*Thanks to Yevgeniy Antipov!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
