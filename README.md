# Modern Cosmology Ed. 2 Corrections

**Please see the corrections [here](Corrections.md).**

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
