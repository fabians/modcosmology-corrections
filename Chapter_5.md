# Chapter 5

## Sec. 5.2

Figure 5.2: the geometry sketch at the bottom of the figure (irrelevant to the content of the figure otherwise) is wrong. It should look like this:

![](images/sketch_ch04_errata.png)

*Thanks to Richard Michalak!*

## Sec. 5.6

The neutrino Boltzmann equation converted to conformal time reads
```math
{{\mathcal{N}}}'(\bm{k},p,\mu,\eta)
+ ik\mu \frac{p}{E_\nu(p)} {\mathcal{N}}
- \textcolor{red}{\frac{a'}{a}} p \frac{\partial}{\partial p} {\mathcal{N}}
= - {{\Phi}}' - i k \mu \frac{E_\nu(p)}{p} {\Psi}.
\tag{5.65}
```  
*Thanks to Joachim Brod!*

##

*(C) 2020-2024, Scott Dodelson and Fabian Schmidt*
